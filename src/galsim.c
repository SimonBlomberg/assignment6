#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "particles.h"
#include "graphics.h"


#include <sys/time.h>

#define W_WIDTH 400
#define W_HEIGHT 400

Quadrouple *qTree = NULL;

static double get_wall_seconds() 
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  double seconds = tv.tv_sec + (double)tv.tv_usec / 1000000;
  return seconds;
}

int main(int argc, char **argv)
{
    
    if(argc != 7)
    {
        printf("Error: invalid input arguments\n");
        return -1;
    }
    // Parse args
    int N = atoi(argv[1]);
    int n_steps = atoi(argv[3]);
    double dt = atof(argv[4]);
    int graphics = atoi(argv[6]);
    double max_theta = atof(argv[5]);

    // Allocate particles 
    Particles parts;
    allocate_particles(&parts, N);
    parts.N_particles = N;

    // Initialize tree
    quad_tree_init(N);

    parse_galaxy_file(argv[2], &parts);
    // Initialize graphics
    if(graphics == 1)
    {
        InitializeGraphics(argv[0], W_WIDTH, W_HEIGHT);
        SetCAxes(0,1);
    }
    time_t before =  get_wall_seconds();

    // Iterate through frames
    int counter = 0;
    while(counter < n_steps)
    {
        counter++;

        // Create tree
        qTree = create_quad_tree(1, 1);

        // Add particles to tree
        for(int i = 0; i < N; i++)
        {
            add_particle(qTree, &parts, i);
        }

        // Update particles
        for(int i = 0; i < parts.N_particles; i++)
        {
            // Parallellize
            update_particle_forces(i, &parts, dt, qTree, max_theta);
        }

        // Update velocities
        for(int i = 0; i < parts.N_particles; i++)
        {
            update_particle_positions(i, &parts, dt);
        }

        free_quad_tree(qTree);

        if(graphics == 1)
        {
            // Draw particles
            ClearScreen();
            for(int i = 0; i < parts.N_particles; i++)
            {
                DrawCircle(parts.posX[i]*W_WIDTH, parts.posY[i]*W_HEIGHT, W_WIDTH, W_HEIGHT, 2, 0);
            }
            Refresh();  
            usleep(3000);
        }
        
    }
    printf("Time taken %.4f\n", get_wall_seconds()-before);
    save_to_galaxy_file("result.gal", &parts);
    free_particles(&parts);
    quad_tree_free();

}