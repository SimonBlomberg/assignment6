#include "particles.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "quad_tree.h"
#include "vec2.h"
void allocate_particles(Particles *parts, int N)
{
    parts->mass = malloc(N*sizeof(double));
    parts->posX = malloc(N*sizeof(double));
    parts->posY = malloc(N*sizeof(double));
    parts->velX = malloc(N*sizeof(double));
    parts->velY = malloc(N*sizeof(double));
    parts->brightness = malloc(N*sizeof(double));
}

void free_particles(Particles *parts)
{
    free(parts->mass);
    free(parts->posX);
    free(parts->posY);
    free(parts->velX);
    free(parts->velY);
    free(parts->brightness);
}

void save_to_galaxy_file(const char *filename, Particles *parts)
{
    FILE *outfile = fopen(filename, "w");


    for(int i = 0; i < parts->N_particles; i++)
    {
        fwrite(&(parts->posX[i]), sizeof(double), 1, outfile);
        fwrite(&(parts->posY[i]), sizeof(double), 1, outfile);
        fwrite(&(parts->mass[i]), sizeof(double), 1, outfile);
        fwrite(&(parts->velX[i]), sizeof(double), 1, outfile);
        fwrite(&(parts->velY[i]), sizeof(double), 1, outfile);
        fwrite(&(parts->brightness[i]), sizeof(double), 1, outfile);
    }

    //fclose(outfile);
    if (fclose(outfile)) 
    { 
        printf("error closing file.");
        exit(-1); 
    }
}

void parse_galaxy_file(const char *filename, Particles *parts)
{
    FILE* infile = fopen(filename, "r");

    if(infile == NULL)
    {
        printf("Error opening file: %s\n", filename);
        exit(-1);
    }

    for(int i = 0; i < parts->N_particles; i++)
    {
        (void)!fread(&(parts->posX[i]), sizeof(double), 1, infile);
        (void)!fread(&(parts->posY[i]), sizeof(double), 1, infile);
        (void)!fread(&(parts->mass[i]), sizeof(double), 1, infile);
        (void)!fread(&(parts->velX[i]), sizeof(double), 1, infile);
        (void)!fread(&(parts->velY[i]), sizeof(double), 1, infile);
        (void)!fread(&(parts->brightness[i]), sizeof(double), 1, infile);
    }

    fclose(infile);
}

void print_particle(int i, Particles *parts)
{
    printf("Particle nr: %d\n\tPosition: (%f, %f)\n\tVelocity: (%f, %f)\n\tMass: %f\n", 
            i,
            parts->posX[i], parts->posY[i], 
            parts->velX[i], parts->velY[i], 
            parts->mass[i]);
}

void update_particle_forces(int i, Particles *parts, double dt, Quadrouple *qTree, double max_theta)
{
    const double G = (100.0/(double)(parts->N_particles));
    double mass = parts->mass[i];

    Vec2 A, F, pos;
    F.x = 0;
    F.y = 0;

    pos.x = parts->posX[i];
    pos.y = parts->posY[i];

    F = mult_vec(G*mass, gravity_component(qTree, parts, pos, i, max_theta));

    A = mult_vec(1/mass, F);

    parts->velX[i] += A.x * dt;
    parts->velY[i] += A.y * dt;

}

void update_particle_positions(int i, Particles *part, double dt)
{
    part->posX[i] += part->velX[i] * dt;
    part->posY[i] += part->velY[i] * dt;
}