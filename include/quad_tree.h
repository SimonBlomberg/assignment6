#pragma once
#include "particles.h"
#include "vec2.h"
#include <stdio.h>
#define epsilon 0.001
typedef struct Particles_t Particles;
typedef struct Quadrouple_t
{
    struct Quadrouple_t *nw;
    struct Quadrouple_t *ne;
    struct Quadrouple_t *sw;
    struct Quadrouple_t *se;
    double width, height;
    Vec2 pos, center_of_mass;
    double mass_sum;
    int particle_id;
    int pool_part;
} Quadrouple;


void quad_tree_init(int in);
void quad_tree_free();
void add_particle(Quadrouple *node, Particles *parts, int particle_id);
Quadrouple *create_quad_tree(double w, double h);
void free_quad_tree(Quadrouple *tree);
void print_tree(Quadrouple *node, Particles *parts, int curr_depth);
Vec2 gravity_component(Quadrouple *qTree, Particles *parts, Vec2 pos, int otherId, double theta);