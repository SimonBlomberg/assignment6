CC=gcc
OBJFILES=src/galsim.o src/particles.o src/graphics.o src/quad_tree.o src/vec2.o
CFLAGS=-I./include -I/opt/X11/include -std=gnu99 -O2 -ffast-math
LDFLAGS=-L/opt/X11/lib -lX11 -lm

galsim: ${OBJFILES}
	${CC}  ${OBJFILES} ${LDFLAGS} -o galsim  

%.o: %.c %.h
	${CC} -c $< ${CFLAGS} -o $@


clean:
	rm -rf src/*.o galsim
