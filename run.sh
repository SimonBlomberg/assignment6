#!/bin/bash


#./galsim 100 ../input_data/ellipse_N_01000.gal 2000 1e-5 0.02 $1
#../compare_gal_files/compare 1000 ../ref_output_data/ellipse_N_01000_after200steps.gal result.gal

particles=2000
timesteps=50

./galsim 5000 ../input_data/ellipse_N_05000.gal 100 1e-5 0.25 0

# ./galsim 50 ../input_data/ellipse_N_00050.gal $timesteps 1e-5 0.0 0
# ./galsim 100 ../input_data/ellipse_N_00100.gal $timesteps 1e-5 0.0 0
# ./galsim 200 ../input_data/ellipse_N_00200.gal $timesteps 1e-5 0.0 0
# ./galsim 400 ../input_data/ellipse_N_00400.gal $timesteps 1e-5 0.0 0
# ./galsim 800 ../input_data/ellipse_N_00800.gal $timesteps 1e-5 0.0 0
# ./galsim 1500 ../input_data/ellipse_N_01500.gal $timesteps 1e-5 0.0 0
# ./galsim 2000 ../input_data/ellipse_N_02000.gal $timesteps 1e-5 0.0 0
# ./galsim 4000 ../input_data/ellipse_N_04000.gal $timesteps 1e-5 0.0 0
# ./galsim 8000 ../input_data/ellipse_N_08000.gal $timesteps 1e-5 0.0 0
# ./galsim 16000 ../input_data/ellipse_N_16000.gal $timesteps 1e-5 0.0 0
#../compare_gal_files/compare $particles result.gal ../ref_output_data/ellipse_N_02000_after200steps.gal
